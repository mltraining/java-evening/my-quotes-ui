import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import QuotesList from '../views/QuotesList.vue'
import QuoteEdit from '../views/QuoteEdit.vue'
import QuoteAdd from '../views/QuoteAdd.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/quotes-list',
    name: 'quotes',
    component: QuotesList
  },
  {
    path: '/quote-edit/:quoteId',
    name: 'quote-edit',
    component: QuoteEdit
  },
  {
    path: '/quote-add',
    name: 'quote-add',
    component: QuoteAdd
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
    }
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
